(TeX-add-style-hook
 "custommath_general"
 (lambda ()
   (TeX-run-style-hooks
    "accents")
   (TeX-add-symbols
    '("SetSymbol" ["argument"] 0)
    '("compc" "Text" ["Text"])
    '("hh*")
    '("hh")
    '("disk*" ["Text"])
    '("disk" ["Text"])
    '("qs*" "Text")
    '("qs" "Text")
    '("gm" ["Text"])
    '("projp" ["Text"])
    '("inj*")
    '("inj")
    '("ext*")
    '("ext")
    '("oc*" "Text")
    '("oc" "Text")
    '("restrict*" "Text" "Text" ["Text"])
    '("restrict" "Text" "Text" ["Text"])
    '("finproj" 1)
    '("latercol" 1)
    '("todocol" 1)
    '("tcomplex" 1)
    '("hhc" 1)
    '("opgreek" 1)
    '("dsnake" 1)
    '("snake" 1)
    '("fixwidedoubletilde" 1)
    '("fixwidetilde" 1)
    '("roof" 1)
    '("fixwidehat" 1)
    '("hacek" 1)
    '("redrg" 1)
    '("dres" 1)
    "mc"
    "mcc"
    "defo"
    "id"
    "sgn"
    "im"
    "maps"
    "isomorphism"
    "lisomorphism"
    "lrisomorphism"
    "smalliso"
    "smallisoleft"
    "lset"
    "rset"
    "sse"
    "spse"
    "ssne"
    "given"
    "pphi"
    "ppi"
    "eepsilon"
    "sheaffont"
    "shc"
    "she"
    "shf"
    "shg"
    "shj"
    "shl"
    "shm"
    "shn"
    "oo"
    "ox"
    "oxx"
    "shp"
    "shx"
    "shom"
    "pp"
    "rchar"
    "tensor"
    "dtensor"
    "rank"
    "Endo"
    "aut"
    "gal"
    "spec"
    "affa"
    "GL"
    "SL"
    "mdr"
    "chh"
    "widehatsym"
    "lowerwidehatsym"
    "widetildesym"
    "lowerwidetildesym"
    "wtilde"
    "tempwhat"
    "pcomp"
    "oline"
    "uline"
    "rgam"
    "rgamc"
    "derd"
    "lderived"
    "rderived"
    "rd"
    "eti"
    "hyh"
    "spece"
    "rhom"
    "cone"
    "extpr"
    "dimtrg"
    "cs"
    "adm"
    "ade"
    "ecat"
    "mor"
    "vectcat"
    "arcat"
    "fun"
    "ww"
    "sw"
    "core"
    "grpd"
    "op"
    "abb"
    "ggt")
   (LaTeX-add-lengths
    "pluslenght"))
 :latex)

