(TeX-add-style-hook
 "sheet5"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "shortnotes"
    "shortnotes10"
    "style-shortnotes"
    "libertikz"
    "custommath_general"
    "custommath_symbols"
    "gitinfo2"
    "eucal"
    "needspace")
   (TeX-add-symbols
    "endtitlepage"
    "square")
   (LaTeX-add-bibliographies
    "lit.bib")
   (LaTeX-add-mathtools-DeclarePairedDelimiters
    '("Brack" "")))
 :latex)

