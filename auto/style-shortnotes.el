(TeX-add-style-hook
 "style-shortnotes"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("babel" "english") ("hyperref" "colorlinks=true" "linkcolor=red!75!black" "" "citecolor=steelblue" "urlcolor=black" "unicode") ("cleveref" "capitalize" "compress")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "babel"
    "csquotes"
    "microtype"
    "lmodern"
    "mathtools"
    "old-arrows"
    "accents"
    "array"
    "stmaryrd"
    "unicode-math"
    "tabularx"
    "booktabs"
    "tikz-cd"
    "enumitem"
    "amsthm"
    "hyperref"
    "cleveref"
    "thmtools"
    "alphalph"
    "xparse")
   (TeX-add-symbols
    '("stackstag" 1)
    "Attention"
    "thisproofname"
    "thisproofqed"
    "locit"
    "openbox")
   (LaTeX-add-environments
    '("proof*" LaTeX-env-args ["argument"] 0)
    '("proof" LaTeX-env-args ["argument"] 0))
   (LaTeX-add-counters
    "everything"
    "exercisecounter"
    "inproof"
    "proofcase"
    "intext"
    "todocount")
   (LaTeX-add-enumitem-newlists
    '("itemize*" "itemize"))
   (LaTeX-add-thmtools-declaretheoremstyles
    "mydef"
    "myexo"
    "myexol"
    "myintext"
    "mycase"
    "mythm"
    "claimstyle"
    "todostyle"
    "errorstyle"
    "proof")
   (LaTeX-add-thmtools-declaretheorems
    "example"
    "cor"
    "numtext"
    "construction"
    "defn"
    "defprop"
    "rem"
    "question"
    "fact"
    "warning"
    "setup"
    "litnote"
    "claim"
    "reduction"
    "step"
    "fact*"
    "claim*"
    "incor"
    "indefn"
    "inlem"
    "inthm"
    "inprop"
    "inrem"
    "inclaim"
    "inexample*"
    "inrem*"
    "inthm*"
    "case"
    "theorem"
    "conjecture"
    "lem"
    "prop"
    "exo"
    "exo*"
    "exol"
    "gproof"
    "todo"
    "danger"))
 :latex)

