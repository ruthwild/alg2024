(TeX-add-style-hook
 "shortnotes"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("xcolor" "dvipsnames") ("scrlayer-scrpage" "autooneside=false" "automark") ("biblatex" "	backend=biber" "	style=alphabetic" "	dateabbrev=false" "	urldate=long" "backref=false" "firstinits=true" "")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "scrartcl"
    "scrartcl10"
    "xcolor"
    "scrlayer-scrpage"
    "style-shortnotes"
    "etoolbox"
    "xpatch"
    "biblatex"
    "blindtext")
   (TeX-add-symbols
    "ParagraphOrNot"
    "abstracttext")
   (LaTeX-add-environments
    '("proof*" LaTeX-env-args ["argument"] 0)
    '("proof" LaTeX-env-args ["argument"] 0))
   (LaTeX-add-lengths
    "thmsep"))
 :latex)

