(TeX-add-style-hook
 "sheet0"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "shortnotes"
    "shortnotes10"
    "style-shortnotes"
    "libertikz"
    "custommath_general"
    "custommath_symbols"
    "gitinfo2"
    "eucal"
    "circledsteps")
   (TeX-add-symbols
    "endtitlepage"
    "square")
   (LaTeX-add-bibliographies
    "lit.bib"))
 :latex)

