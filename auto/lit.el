(TeX-add-style-hook
 "lit"
 (lambda ()
   (LaTeX-add-bibitems
    "k-theory-lecture-notes"
    "dyckerhoff-kapranov"
    "dyckerhoff-lectures-on-hall-algebras"
    "local-fields"
    "neukirch"
    "stacks1"))
 '(or :bibtex :latex))

